package commands

import (
	"fmt"
	"math"
	"strconv"
	"strings"

	"github.com/bwmarrin/discordgo"
)

var CalcHelp = "Calculate result for 2 numbers. Can use +, -, *, / operations. Usage: `!calc <number> + <number>`."

func Calc(args []string, session *discordgo.Session, message *discordgo.Message) string {
	if len(args) < 3 {
		return "Invalid arguments. Usage: `!calc <number> + <number>. Supports +, -, *, / operations."
	}
	sigfig := getSigFigCount(args[0], args[2])
	left, err := strconv.ParseFloat(args[0], 64)
	if err != nil {
		return "!calc can only be used with numbers."
	}
	operator := args[1]
	right, err := strconv.ParseFloat(args[2], 64)
	if err != nil {
		return "!calc can only be used with numbers."
	}

	switch operator {
	case "+":
		return fmt.Sprint(RoundTo(left+right, sigfig))
	case "-":
		return fmt.Sprint(RoundTo(left-right, sigfig))
	case "*":
		return fmt.Sprint(RoundTo(left*right, sigfig))
	case "/":
		if right == 0 {
			return "Invalid operation. Cannot divide by 0."
		}
		return fmt.Sprint(left / right)
	default:
		return "Invalid operator. Valid operators are +, -, *, /"
	}
}

func getSigFigCount(a string, b string) int {
	av := 0.0
	// split on decimal of a number string
	asplit := strings.Split(a, ".")
	// Check if decimal exists
	if len(asplit) > 1 {
		// Convert the number of decimal places to a float
		av = float64(len(asplit[1]))
	}

	bv := 0.0
	// split on decimal of a number string
	bsplit := strings.Split(b, ".")
	// Check if decimal exists
	if len(bsplit) > 1 {
		// Convert the number of decimal places to a float
		bv = float64(len(bsplit[1]))
	}

	// Return the greater number of decimal places for either number
	return int(math.Max(av, bv))
}

func RoundTo(value float64, places int) float64 {
	return math.Round(value*math.Pow10(places)) / math.Pow10(places)
}
