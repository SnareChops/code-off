package commands

import (
	"fmt"

	"../models"
	"github.com/bwmarrin/discordgo"
)

var WhatsHotHelp = "Displays the most commonly used words in the server over the last 24 hours"

func WhatsHot(args []string, session *discordgo.Session, message *discordgo.Message) string {

	// lookup most common word
	// word must be a word, not emote, symbol, or number

	results, err := models.GetFrequencies(message.ChannelID)
	if err != nil {
		return fmt.Sprint(err)
	}

	if len(results) > 0 {
		return fmt.Sprintf("The hot topics are: `%s` - %d times, `%s` - %d times, `%s` - %d times", results[0].Word, results[0].Count, results[1].Word, results[1].Count, results[2].Word, results[2].Count)
	}
	return "No frequently words found"
}
