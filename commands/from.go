package commands

import (
	"errors"
	"fmt"
	"strconv"
	"strings"

	"github.com/bwmarrin/discordgo"
)

const (
	DISTANCE = "distance"
	VOLUME   = "volume"
	TEMP     = "temperature"
)

var FromHelp = "Convert values between units. Usage: `!from <number> <unit> to <unit>`"

func From(args []string, session *discordgo.Session, message *discordgo.Message) string {

	if len(args) < 4 {
		return "Invalid format. Usage: `!from <number> <unit> to <unit>`"
	}

	float, err := strconv.ParseFloat(args[0], 64)
	if err != nil {
		return "Invalid number format"
	}
	converted, t, err := convertToCommon(float, args[1])
	if err != nil {
		return fmt.Sprint(err)
	}
	result, err := convertToTarget(converted, args[3], t)
	if err != nil {
		return fmt.Sprint(err)
	}
	return fmt.Sprintf("%f%s", result, args[3])
}

func convertToTarget(float float64, unit string, t string) (float64, error) {
	switch strings.ToLower(unit) {
	case "mm":
		if t != DISTANCE {
			return 0, errors.New(fmt.Sprintf("Cannot convert %s to %s", t, DISTANCE))
		}
		float *= 1000.0
		return float, nil
	case "cm":
		if t != DISTANCE {
			return 0, errors.New(fmt.Sprintf("Cannot convert %s to %s", t, DISTANCE))
		}
		float *= 100.0
		return float, nil
	case "dm":
		if t != DISTANCE {
			return 0, errors.New(fmt.Sprintf("Cannot convert %s to %s", t, DISTANCE))
		}
		float *= 10.0
		return float, nil
	case "m":
		if t != DISTANCE {
			return 0, errors.New(fmt.Sprintf("Cannot convert %s to %s", t, DISTANCE))
		}
		return float, nil
	case "km":
		if t != DISTANCE {
			return 0, errors.New(fmt.Sprintf("Cannot convert %s to %s", t, DISTANCE))
		}
		float /= 1000.0
		return float, nil
	case "in":
		if t != DISTANCE {
			return 0, errors.New(fmt.Sprintf("Cannot convert %s to %s", t, DISTANCE))
		}
		float *= 39.37
		return float, nil
	case "ft":
		if t != DISTANCE {
			return 0, errors.New(fmt.Sprintf("Cannot convert %s to %s", t, DISTANCE))
		}
		float *= 3.281
		return float, nil
	case "yd":
		if t != DISTANCE {
			return 0, errors.New(fmt.Sprintf("Cannot convert %s to %s", t, DISTANCE))
		}
		float *= 1.094
		return float, nil
		////
	case "ml":
		if t != VOLUME {
			return 0, errors.New(fmt.Sprintf("Cannot convert %s to %s", t, VOLUME))
		}
		return float, nil
	case "l":
		if t != VOLUME {
			return 0, errors.New(fmt.Sprintf("Cannot convert %s to %s", t, VOLUME))
		}
		float /= 1000.0
		return float, nil
	case "oz":
		if t != VOLUME {
			return 0, errors.New(fmt.Sprintf("Cannot convert %s to %s", t, VOLUME))
		}
		float /= 29.574
		return float, nil
	case "qt":
		if t != VOLUME {
			return 0, errors.New(fmt.Sprintf("Cannot convert %s to %s", t, VOLUME))
		}
		float /= 946.353
		return float, nil
	case "gal":
		if t != VOLUME {
			return 0, errors.New(fmt.Sprintf("Cannot convert %s to %s", t, VOLUME))
		}
		float /= 3785.412
		return float, nil
	case "g":
		if t != VOLUME {
			return 0, errors.New(fmt.Sprintf("Cannot convert %s to %s", t, VOLUME))
		}
		float /= 3785.412
		return float, nil
		////
	case "c":
		if t != TEMP {
			return 0, errors.New(fmt.Sprintf("Cannot convert %s to %s", t, TEMP))
		}
		return float, nil
	case "f":
		if t != TEMP {
			return 0, errors.New(fmt.Sprintf("Cannot convert %s to %s", t, TEMP))
		}
		float = (float * (9.0 / 5.0)) + 32
		return float, nil
	case "k":
		if t != TEMP {
			return 0, errors.New(fmt.Sprintf("Cannot convert %s to %s", t, TEMP))
		}
		float += 273.15
		return float, nil
	default:
		return 0, errors.New("Invalid unit for conversion")
	}
}

func convertToCommon(float float64, unit string) (float64, string, error) {
	switch strings.ToLower(unit) {
	case "mm":
		// convert to m
		float /= 1000.0
		return float, DISTANCE, nil
	case "cm":
		// convert to m
		float /= 100.0
		return float, DISTANCE, nil
	case "dm":
		// convert to m
		float /= 10.0
		return float, DISTANCE, nil
	case "m":
		// is m
		return float, DISTANCE, nil
	case "km":
		// convert to m
		float *= 1000.0
		return float, DISTANCE, nil
	case "in":
		// convert to m
		float /= 39.37
		return float, DISTANCE, nil
	case "ft":
		// convert to m
		float /= 3.281
		return float, DISTANCE, nil
	case "yd":
		// convert to m
		float /= 1.094
		return float, DISTANCE, nil

		////
	case "ml":
		// is ml
		return float, VOLUME, nil
	case "cc":
		// is
		return float, VOLUME, nil
	case "l":
		// convert to ml
		float /= 1000.0
		return float, VOLUME, nil
	case "oz":
		// convert to ml
		float *= 29.574
		return float, VOLUME, nil
	case "qt":
		// convert to ml
		float *= 946.353
		return float, VOLUME, nil
	case "gal":
		// convert to ml
		float *= 3785.412
		return float, VOLUME, nil
	case "g":
		// convert to ml
		float *= 3785.412
		return float, VOLUME, nil

		////
	case "c":
		// is c
		return float, TEMP, nil
	case "f":
		// convert to c
		float = (float - 32.0) * (5.0 / 9.0)
		return float, TEMP, nil
	case "k":
		// convert to c
		float -= 273.15
		return float, TEMP, nil
	default:
		return 0, "", errors.New("Invalid unit for conversion")
	}
}
