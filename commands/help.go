package commands

import (
	"fmt"
	"strings"

	"github.com/bwmarrin/discordgo"

	"../runners"
)

var HelpHelp = "You're silly"

func Help(args []string, session *discordgo.Session, message *discordgo.Message) string {

	if len(args) == 0 {
		// list out all available commands
		names := runners.GetCommandNames()
		list := strings.Join(names, "\n- ")
		return fmt.Sprintf("Commands:\n- %s", list)
	}
	// show help for args[1]
	help := runners.LookupCommand(args[0])
	if help == nil {
		return fmt.Sprintf("No command %s found. Use `!help` to get a list of available commands.", args[0])
	}
	return help.Help
}
