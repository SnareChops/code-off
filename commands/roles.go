package commands

import (
	"fmt"
	"strings"

	"../models"
	"github.com/bwmarrin/discordgo"
)

var RolesHelp = "Allows you to assign roles to yourself or others (if admin). Usage `!roles giveme <role>`, `!roles giveto <user> <role>`, `!roles addrole <role>`, `!roles remrole <role>`."

// Roles The !roles command implementation
func Roles(args []string, session *discordgo.Session, message *discordgo.Message) string {
	if len(args) == 0 {
		return ListRoles(message.GuildID)

	}
	switch args[0] {
	case "giveme":
		return GiveMe(args[1:len(args)], session, message)
	case "giveto":
		return GiveTo(args[1:len(args)], session, message)
	case "addrole":
		return AddRole(args[1:len(args)], session, message)
	case "remrole":
		return RemRole(args[1:len(args)], session, message)
	default:
		return "Invalid subcommand. Options are `giveme`, `giveto`, `addrole`, and `remrole`."
	}
}

func ListRoles(server string) string {
	roles, err := models.FindRoles(server)
	if err != nil {
		return fmt.Sprint(err)
	}

	if len(roles) == 0 {
		return "No assignable roles found"
	}

	names := []string{}
	for _, role := range roles {
		names = append(names, role.Name)
	}
	list := strings.Join(names, "\n- ")
	return fmt.Sprintf("**Available roles**:\n- %s", list)
}

func GiveMe(args []string, session *discordgo.Session, message *discordgo.Message) string {
	if len(args) == 0 {
		return "Missing role. Usage `!roles giveme <rolename>`."
	}
	name := args[0]
	// attempt to assign role to user
	role, err := models.FindRole(message.GuildID, name)
	if err != nil {
		return fmt.Sprint(err)
	}
	if role == nil {
		return fmt.Sprintf("The %s role is not self assignable or does not exist", name)
	}
	fmt.Printf("Adding %s to %s in %s", *role, message.Author.ID, message.GuildID)
	err = session.GuildMemberRoleAdd(message.GuildID, message.Author.ID, role.ID)
	if err != nil {
		return fmt.Sprintf("Insufficient permission to give role %s to %s", name, message.Author.Username)
	}
	// respond
	return fmt.Sprintf("*%s received the %s role*", message.Author.Username, role.Formal)
}

// GiveTo
// !roles giveto <user> <role>
func GiveTo(args []string, session *discordgo.Session, message *discordgo.Message) string {
	if len(args) < 2 {
		return "Missing arguments. Usage `!roles giveto <user> <role>`."
	}

	user := strings.TrimPrefix(strings.TrimSuffix(args[0], ">"), "<@")
	name := args[1]
	role, err := models.FindRole(message.GuildID, name)
	if role == nil {
		return fmt.Sprintf("The %s role is not assignable or does not exist", name)
	}
	fmt.Printf("Adding %s to %s in %s from %s", *role, user, message.GuildID, message.Author.ID)
	err = session.GuildMemberRoleAdd(message.GuildID, user, role.ID)
	if err != nil {
		return fmt.Sprintf("Insufficient permission to give role %s to %s", name, args[0])
	}
	// respond
	return fmt.Sprintf("*%s received the %s role from %s*", message.Author.Username, name, message.Author.Username)

}

func AddRole(args []string, session *discordgo.Session, message *discordgo.Message) string {
	if len(args) == 0 {
		return "Missing role. Usage `!roles addrole <rolename>`."
	}
	name := args[0]
	roleList, err := session.GuildRoles(message.GuildID)
	if err != nil {
		return fmt.Sprint(err)
	}
	for _, role := range roleList {
		if strings.ToLower(role.Name) == strings.Replace(name, "_", " ", -1) {
			err := models.SaveRole(message.GuildID, name, role.ID, role.Name)
			if err != nil {
				return fmt.Sprint(err)
			}
			return fmt.Sprintf("%s added %s to accessible roles", message.Author.Username, role.Name)
		}
	}
	return fmt.Sprintf("%s role not found in this server", name)
}

func RemRole(args []string, session *discordgo.Session, message *discordgo.Message) string {
	if len(args) == 0 {
		return "Missing role. Usage `!roles remrole <role>`."
	}

	if role, err := models.FindRole(message.GuildID, args[0]); role == nil && err == nil {
		return fmt.Sprintf("%s role does not exist in assignable roles.", args[0])
	} else if err != nil {
		return fmt.Sprint(err)
	}

	err := models.DestroyRole(message.GuildID, args[0])
	if err != nil {
		return fmt.Sprint(err)
	}

	return fmt.Sprintf("%s removed %s from accessable roles", message.Author.Username, args[0])
}
