package models

import "time"

type Word struct {
	ID        uint   `db:"id"`
	Word      string `db:"word"`
	Server    string `db:"server"`
	Timestamp int64  `db:"timestamp"`
}

type WordFrequency struct {
	Word  string `db:"word"`
	Count int    `db:"count"`
}

func SaveWord(server string, word string) error {
	now := time.Now().Unix()
	_, err := DB.NamedExec("INSERT INTO `words` (`word`,`server`,`timestamp`) VALUES (:word,:server,:timestamp)", Word{Word: word, Server: server, Timestamp: now})
	if err != nil {
		return err
	}
	return nil
}

func GetFrequencies(server string) ([]WordFrequency, error) {
	results := []WordFrequency{}
	since := time.Now().AddDate(0, 0, -1)
	err := DB.Select(&results, "SELECT `word` as `word`,count(*) as `count` FROM `words` WHERE `timestamp` > ? GROUP BY `word` ORDER BY `count` DESC", since)
	if err != nil {
		return nil, err
	}
	return results, nil
}
