package models

import (
	"fmt"
	"strings"
)

type Role struct {
	Server string `db:"server"`
	Name   string `db:"name"`
	ID     string `db:"id"`
	Formal string `db:"formal"`
}

func SaveRole(server string, name string, id string, formal string) error {
	role := Role{
		Server: server,
		Name:   name,
		ID:     id,
		Formal: formal,
	}
	_, err := DB.NamedExec("INSERT INTO `roles` (`server`,`name`,`id`,`formal`) VALUES (:server,:name,:id,:formal)", role)
	return err
}

func FindRole(server string, name string) (*Role, error) {
	role := Role{}
	err := DB.Get(&role, "SELECT * FROM `roles` WHERE `server`=? AND `name`=?", server, name)
	if err != nil {
		if strings.Contains(fmt.Sprint(err), "no rows") {
			return nil, nil
		}
		return nil, err
	}
	return &role, nil
}

func FindRoles(server string) ([]Role, error) {
	roles := []Role{}
	err := DB.Select(&roles, "SELECT * FROM `roles` WHERE `server`=?", server)
	if err != nil {
		return nil, err
	}
	return roles, err
}

func DestroyRole(server string, name string) error {
	_, err := DB.Exec("DELETE FROM `roles` WHERE `server`=? AND `name`=?", server, name)
	if err != nil {
		return err
	}
	return nil
}
