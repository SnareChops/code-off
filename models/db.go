package models

import (
	"errors"
	"log"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"
)

type Migration func(*sqlx.Tx) error
type Database struct {
	*sqlx.DB
	Name           string
	User           string
	Password       string
	Address        string
	MigrationTable string
	Migrations     map[string]Migration
}

var DB *Database

func (db *Database) ConnectMySql() error {
	conn := db.User + ":" + db.Password + "@tcp(" + db.Address + ")/" + db.Name + "?charset=utf8mb4,utf8&parseTime=True"
	var err = errors.New("Attempting DB Connection")
	var retries = 0
	for err != nil && retries < 30 {
		log.Print(err)
		retries++
		time.Sleep(2 * time.Second)
		println("Attempting DB Connection...")
		db.DB, err = sqlx.Connect("mysql", conn)
	}
	if err != nil {
		return err
	}
	return nil
}

func (db *Database) ConnectSqlite() error {
	var err error
	db.DB, err = sqlx.Connect("sqlite3", ":memory:")
	if err != nil {
		return err
	}
	return nil
}

func (db *Database) RunMigrations() error {
	println("Running migrations...")
	if len(db.MigrationTable) != 0 {
		db.MustExec("CREATE TABLE IF NOT EXISTS `" + db.MigrationTable + "` (`name` varchar(255), `timestamp` datetime, PRIMARY KEY(`name`))")
		existing := []string{}
		if err := db.Select(&existing, "SELECT `name` FROM `"+db.MigrationTable+"`"); err != nil {
			return err
		}
		if db.Migrations != nil {
			for key, migration := range db.Migrations {
				if migrationExists(existing, key) {
					continue
				}
				println("Running migration " + key + "...")
				tx := db.MustBegin()
				err := migration(tx)
				if err != nil {
					tx.Rollback()
					log.Fatal(err)
				}
				tx.MustExec("INSERT INTO `"+db.MigrationTable+"` (`name`,`timestamp`) VALUES (?,CURRENT_TIMESTAMP)", key)
				tx.Commit()
			}
		}
	}
	return nil
}

func migrationExists(collection []string, name string) bool {
	for _, item := range collection {
		if item == name {
			return true
		}
	}
	return false
}
