package actions

import "github.com/bwmarrin/discordgo"

func SendMessage(session *discordgo.Session, channelID string, message string) {
	session.ChannelMessageSend(channelID, message)
}
