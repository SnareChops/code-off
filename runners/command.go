package runners

import (
	"regexp"
	"strings"

	"github.com/bwmarrin/discordgo"
)

type CommandHandler func(args []string, session *discordgo.Session, message *discordgo.Message) string
type CommandInfo struct {
	Handler CommandHandler
	Help    string
}

var commandHandlers = map[string]*CommandInfo{}

func RegisterCommand(name string, handler CommandHandler, help string) {
	commandHandlers[name] = &CommandInfo{
		Handler: handler,
		Help:    help,
	}
}

func LookupCommand(name string) *CommandInfo {
	return commandHandlers[name]
}

func DetectCommand(m *discordgo.MessageCreate) (*string, []string) {
	// Must start with !
	regex := regexp.MustCompile(`^!.*`)
	if regex.MatchString(m.Content) {
		args := []string{}
		var name string
		for i, word := range strings.Split(m.Content, " ") {
			if i == 0 {
				name = word[1:len(word)]
			} else {
				args = append(args, word)
			}
		}
		return &name, args
	}
	return nil, nil
}

func RunCommand(name string, args []string, session *discordgo.Session, message *discordgo.Message) string {
	command := LookupCommand(name)
	if command != nil {
		return command.Handler(args, session, message)
	}
	return ""
}

func GetCommandNames() []string {
	result := []string{}
	for key := range commandHandlers {
		result = append(result, key)
	}
	return result
}
