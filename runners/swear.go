package runners

import (
	"regexp"

	"github.com/bwmarrin/discordgo"
)

func FilterSwears(m *discordgo.MessageCreate) string {
	regex := regexp.MustCompile(`(?m)(?:shit|fuck)`)
	replaced := regex.ReplaceAllStringFunc(m.Content, func(word string) string {
		if len(word) == 4 {
			return word[0:1] + "***"
		}
		return word[0:1] + "***" + word[4:len(word)]
	})
	if m.Content != replaced {
		return replaced
	}
	return ""
}
