package runners

import (
	"regexp"
	"strings"

	"../models"
	"github.com/bwmarrin/discordgo"
)

func LogWords(m *discordgo.Message) {

	// scrape words from message content
	words := strings.Split(m.Content, " ")
	for _, word := range words {
		// save each word to database
		re := regexp.MustCompile(`^[a-zA-Z']+`)
		if value := re.FindString(word); value != "" {
			models.SaveWord(m.GuildID, value)
		}
	}

}
