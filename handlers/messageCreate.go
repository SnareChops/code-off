package handlers

import (
	"fmt"

	"../runners"
	"github.com/bwmarrin/discordgo"
)

func MessageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	// ignore messages from self
	if m.Author.ID == s.State.User.ID {
		return
	}
	println(m.Message.Content)

	command, args := runners.DetectCommand(m)
	if command != nil {
		response := runners.RunCommand(*command, args, s, m.Message)
		if response != "" {
			s.ChannelMessageSend(m.ChannelID, response)
		}
	} else {
		response := runners.FilterSwears(m)
		if response != "" {
			s.ChannelMessageDelete(m.ChannelID, m.Message.ID)
			s.ChannelMessageSend(m.ChannelID, fmt.Sprintf("**%s**: %s", m.Author.Username, response))
		}

		go runners.LogWords(m.Message)
	}
}
