package migrations

import "github.com/jmoiron/sqlx"

func CreateRoleTable(tx *sqlx.Tx) error {
	_, err := tx.Exec("CREATE TABLE `roles` (`server` varchar(255) NOT NULL, `name` varchar(255) NOT NULL, `id` varchar(255) NOT NULL, `formal` varchar(255) NOT NULL, PRIMARY KEY(`server`,`id`))")
	return err
}
