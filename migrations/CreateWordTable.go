package migrations

import "github.com/jmoiron/sqlx"

func CreateWordTable(tx *sqlx.Tx) error {
	_, err := tx.Exec("CREATE TABLE `words` (`id` int UNSIGNED NOT NULL AUTO_INCREMENT, `word` varchar(255), `server` varchar(255), `timestamp` bigint UNSIGNED NOT NULL, PRIMARY KEY(`id`))")
	return err
}
