package main

import (
	"os"
	"os/signal"
	"syscall"

	"./commands"
	"./handlers"
	"./migrations"
	"./models"
	"./runners"
	"github.com/bwmarrin/discordgo"
)

func fromEnv(name string, def string) string {
	if env := os.Getenv(name); env != "" {
		return env
	}
	return def
}

func main() {

	models.DB = &models.Database{
		User:           fromEnv("DB_USER", "root"),
		Password:       fromEnv("DB_PWORD", "root"),
		Address:        fromEnv("DB_ADDRESS", "127.0.0.1:3307"),
		Name:           fromEnv("DB_NAME", "code-off"),
		MigrationTable: "__migrations",
		Migrations: map[string]models.Migration{
			"CreateWordTable": migrations.CreateWordTable,
			"CreateRoleTable": migrations.CreateRoleTable,
		},
	}

	runners.RegisterCommand("calc", commands.Calc, commands.CalcHelp)
	runners.RegisterCommand("help", commands.Help, commands.HelpHelp)
	runners.RegisterCommand("roles", commands.Roles, commands.RolesHelp)
	runners.RegisterCommand("whatshot", commands.WhatsHot, commands.WhatsHotHelp)
	runners.RegisterCommand("from", commands.From, commands.FromHelp)

	err := models.DB.ConnectMySql()
	if err != nil {
		panic(err)
	}

	err = models.DB.RunMigrations()
	if err != nil {
		panic(err)
	}

	d, err := discordgo.New("Bot " + os.Getenv("DISCORD_TOKEN"))
	if err != nil {
		panic(err)
	}

	d.AddHandler(handlers.MessageCreate)
	err = d.Open()
	if err != nil {
		panic(err)
	}
	defer d.Close()

	println("Running")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc
}
